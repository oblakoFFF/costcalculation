object Form1: TForm1
  Left = 308
  Top = 135
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = 'Form1'
  ClientHeight = 446
  ClientWidth = 677
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object StringGrid1: TStringGrid
    Left = 56
    Top = 24
    Width = 500
    Height = 201
    Cursor = crIBeam
    BorderStyle = bsNone
    ColCount = 4
    FixedCols = 0
    RowCount = 16
    Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goDrawFocusSelected, goRowSizing, goColSizing, goEditing]
    ParentShowHint = False
    ScrollBars = ssNone
    ShowHint = False
    TabOrder = 0
    OnClick = StringGrid1Click
    OnDrawCell = StringGrid1DrawCell
    OnKeyPress = StringGrid1KeyPress
    OnSelectCell = StringGrid1SelectCell
  end
  object Button1: TButton
    Left = 288
    Top = 264
    Width = 75
    Height = 25
    Caption = 'Button1'
    ModalResult = 1
    TabOrder = 1
    OnClick = Button1Click
  end
  object Edit1: TEdit
    Left = 392
    Top = 264
    Width = 121
    Height = 21
    BiDiMode = bdRightToLeftReadingOnly
    ParentBiDiMode = False
    ReadOnly = True
    TabOrder = 2
    Text = 'Edit1'
  end
  object Button2: TButton
    Left = 248
    Top = 360
    Width = 75
    Height = 25
    Caption = 'Button2'
    TabOrder = 3
    OnClick = Button2Click
  end
end
